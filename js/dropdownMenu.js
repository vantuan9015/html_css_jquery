jQuery(document).ready(function($){
	var flag = true;

	$(window).resize(function(){
		var widthBrowser = $(window).width();
		if (flag){
			if (widthBrowser <= 768) {
				flag = false;
				$("<p id='dropdown'>DropDown</p>").insertAfter("div#header-wrap div.menu-nav-search");
				$("ul#mobile-menu-show").insertAfter("div#header-wrap p#dropdown");
			}
		}
		if (widthBrowser > 768) {
			flag = true;
			$("p#dropdown").remove();
			$("ul#mobile-menu-show").insertBefore("div#header-wrap div.menu-nav-search p.search");
		}

		// Show and hide menu
		if (!flag) {
			// style button
			$("#dropdown").css({
				"display": "block",
				"width": "60px", 
				"height": "30px",
				"background": "#f1f1f1",
				"overflow": "hidden"
			});

			$("#dropdown").click(function(){
				if (! $('ul#mobile-menu-show').is(':visible'))
				{
					$("ul#mobile-menu-show").css({
						"display": "block"
					});
				} else {
					$("ul#mobile-menu-show").css({
						"display": "none"
					});
				}
			});
		} else {
			$("ul#mobile-menu-show").show();
		}

		// Process click to toggle dropdown menu
	});
});